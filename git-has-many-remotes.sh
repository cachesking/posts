---
title: Git Has Many SSH
updated:
categories: tools
tags: git
keywords: git
excerpt: git & ssh config for multiple hosts
---

# Managing many git ssh configs

You\'re ready for this code to be "out there". Easy enough. You fire up
your trusted internet search tool and go with the first `git**b` you see.
You might use this approach is you have many git**b users on the same machine.

We will add an AWS example at the end to belabor the point.

## Update git config

With many ssh configs I typically avoid global configs. Without a global
git user, git will ask you to update your local config
the first time you try to run `git init`.

`git config --local set user.name coolname`

Services typically provide a no reply email to mask our personal email.

`git config --local set user.email some@email.com`

Validate that your commands took: `cat .git/config`

## Checking for existing keys

[github](https://docs.github.com/en/github/authenticating-to-github/checking-for-existing-gpg-keys)

## Create Keys

[github](https://docs.github.com/en/github/authenticating-to-github/generating-a-new-gpg-key)
[gitlab](https://gitlab.com/help/ssh/README)

## Add SSH Keys

[github](https://github.com/settings/keys)
[gitlab](https://gitlab.com/profile/keys)

## Update SSH Config

Add a host alias for each `git**b` to `~/.ssh/config`.

 ```
 Host github
   Hostname github.com
   User git

 Host gitlab
  Hostname gitlab.com
  User git

 Host *
   PreferredAuthentications publickey
   IdentityFile ~/.ssh/id_rsa
 ```

 _still not sure if `AddKeysToAgent yes` is needed_

## Measure twice. Cut once.

Use a verbose test to verify each user: `ssh -vT alias`

## Add remote

List the git repositories remote rempositories.

`git remote -v`

_you may need to replace remotes with their alias_

Using the name provided to `~/.ssh/config` interpolates the correct `Hostname`.

  - `cd` to project root
  - `git remote add remote1 ssh://awscodecommit/v1/repos/my-repo-name`
  - `git remote add remote2 git@github:username/my-repo-name.git`
  - `git remote add remote3 git@gitlab:username/my-repo-name.git`

_please pick more appriopriate names_

## But you said cloud!

Create an IAM user with the appropriate
[permissions](https://docs.aws.amazon.com/codecommit/latest/userguide/auth-and-access-control-permissions-reference.html)
to commit to AWS CodeCommit repository and
[add](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_ssh-keys.html)
your ssh key to you IAM user.

Update `~/.ssh/config` with your AWS IAM user key and host name.

 ```
 Host awscodecommit
   Hostname git-codecommit.region.amazonaws.com
   User aws access key id
 ```

Done! Don\'t forget to test with `ssh -vT your-alias`

