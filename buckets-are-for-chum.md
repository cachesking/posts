---
title: "Buckets are for Chum"
updated:
categories:
tags:
keywords:
excerpt: "Get in chum. We hosting single page websites in Lambda"
---

Hosting static s3 sites is like, the thing to do. As soon as AWS released site
hosting from S3 I was hooked. I'm a word press hater. That's harsh of me. WP is
pretty dope actually. PHP ain't bad either. Truth be told, it's the servers
that I dislike.

[Gatsby](https://www.gatsbyjs.org/), a react based static site generator, was recently [backed](https://www.forbes.com/sites/davidjeans/2020/05/27/gatsby-website-building-startup-backed-by-index-ventures-raises-28-million/#79ea83a47f3e). [There are so many](https://www.staticgen.com/) static site generators is dizzying.

AWS [announced](https://aws.amazon.com/about-aws/whats-new/2011/02/17/Amazon-S3-Website-Features/) support for S3 Websites in early 2011 and it's been my preferred method for
hosting single page applications ever since.

My confidence in this decision is bolstered by the growing number of SaaS
companies in this space. [Netlify](https://www.netlify.com/products/) offers a [free](https://www.netlify.com/pricing/) tier that is very inclusive. Automated
builds from git and deploying to a global Edge Network are exactly the types of
things we're looking for in a static host. That isn't enough to pull me away
from [GitHub Pages](https://pages.github.com/) or [GitLab's](https://docs.gitlab.com/ee/user/project/pages/) equivalent.

Netlify's next tier, a $19 dollar a month commitment, is very enticing. Password-
protected sites, slack notifications, build minutes (seriously?), and audit logs
got me thinking it's time to drop a few Tubman's.

Nah, fam. Native cloud serverless buzzwords are here to save you. Searching for
`cloud front distribution for s3 website` yields a slew of great content. A few
[videos](https://youtu.be/DiIaoIcoKNY) and a great [blog post](https://nickolaskraus.org/articles/creating-a-static-website-using-cloudformation/).

Boom we're hosting static sites.

If I'm being honest with myself, this is not easy to grok. Deploying CloudFront
Distributions takes upwards of ten minutes. AWS certificate manager needs
a the certificate to be verified. We need an origin identity, a bucket, and
bucket policy. Sheesh. There has to be an easier way. There is. It's not perfect.
It has limitations depending on how deep into the weeds we want to get.

I love playing with this idea and I've been trying to create content management
systems out of [nothing](https://github.com//hexo-plugin) for a long time. My
personal recommendation is to use Netlify and if you are really set on doing it
yourself, use Nickolas Kraus' blog from
[above](https://nickolaskraus.org/articles/creating-a-static-website-using-cloudformation/).
I would also  reach for [AWS CDK](https://docs.aws.amazon.com/cdk/latest/guide/getting_started.html) for reasons I'll touch on when we address Lambda@Edge and using TypeScript.

## Getting Started

What you'll need to get started:

AWS account
AWS CLI
AWS SAM (optional)

```go
package main

import (
    "bytes"
    "html/template"

    "github.com/aws/aws-lambda-go/events"
    "github.com/aws/aws-lambda-go/lambda"
)

const html = `
<!DOCTYPE html>
<html>
    <head>
        <title>Hello World</title>
    </head>
    <body>
        <h1>Hello from AWS Lambda</h1>
    </body>
</html>
`

var (
    // Buffer contains the io.Writer interface needed by template.Execute
    buf bytes.Buffer

    // Must will handle errors. This allows us to declare a new template
    // outside of the Lambda Handler. Important for speed and caching.
    t = template.Must(template.New("HTML Page").Parse(html))

    // Default return Headers
    headers = map[string]string{"Content-Type": "text/html"}
)

func init() {
    t.Execute(&buf, nil)
}

// https://docs.aws.amazon.com/lambda/latest/dg/best-practices.html
func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
    return events.APIGatewayProxyResponse{
        Headers:    headers,
        Body:       buf.String(),
        StatusCode: 200,
    }, nil
}

func main() {
    lambda.Start(handler)
}
```

## SAM Template

```yaml
AWSTemplateFormatVersion: '2010-09-09'
Transform: AWS::Serverless-2016-10-31

Resources:
  TextHtmlFunction:
    Type: AWS::Serverless::Function
    Properties:
      CodeUri: index/
      Handler: index
      Events:
        IndexPage:
          Type: Api
          Properties:
            Path: /
            Method: GET
```

If we have sam local we can test this out with `sam local start-api`.

When deploying for the first time run `sam deploy --guided`.

## Conclusion

Still a Work In Progress. Use Python or JavaScript for reasons. Go is fun though.

### Outline

- generators
  - gatsby (js)
  - hugo (go)
  - jekyll (ruby)

- hosts
  - netlify
  - s3 bucket

My personal go to for a really long time

- s3

- cd/cd

Trash that:

- cloud front distros take 20 minutes

- s3 buckets sync and aws resources disjointed

New approach:

- quick solution
  - body content-type text/html
  - html templates generated in lambda
    - go (personal favorite)

- benefits
  - apigw caches GET requests by default
  - AWS logging and contextual lambda invocation

- limitations
  - go (no access to static)
    - difficult to access static assets
    - can't use lambda at edge (js and py)
  - api gateway regional vs s3 global distribution

- next step
  - JavaScript Lambda@Edge spa

